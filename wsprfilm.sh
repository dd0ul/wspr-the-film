#!/bin/sh

# wspr_film.sh
# version 0.2
# (C) Michael Renner <dd0ul@darc.de>
# Create a video of the map on wsprnet.org with simple tools.
# https://gitlab.com/dd0ul/wspr-the-film

BAND=$1

# check if firefox entry exists

ENTRY=$(grep "Name=${BAND}" ~/.mozilla/firefox/profiles.ini)
if [ "${ENTRY}" = "" ] ; then
	echo "no entry ${BAND} found in ~/.mozilla/firefox/profiles.ini"
	echo "please generate one"
	exit 65
else
	echo "entry ${BAND} found in ~/.mozilla/firefox/profiles.ini"
	NUMMER=$(grep -B 1 "Name=${BAND}" ~/.mozilla/firefox/profiles.ini | head -n 1 | sed s/Profile// | sed 's|[^0-9]||g')
	echo "starting screen No ${NUMMER}"
fi

RES=2560x1080
FR=0.166666666666666666
Xephyr :${NUMMER} -ac -br -reset -terminate -listen tcp -title "WSPR ${BAND}" -screen ${RES} -reset -terminate &
sleep 5
cat << EOF > /tmp/cmd_${BAND}.sh

#!/bin/bash
export DISPLAY=:${NUMMER}
kwin &
firefox -P ${BAND} https://wsprnet.org/drupal/wsprnet/map &
sleep 60
TZ=London xclock -update 60 -render -bg MediumAquamarine -geometry 150x150-90+710 &
mkdir -p ~/wsprfilm
rm -f ~/wsprfilm/${BAND}.mkv
ffmpeg -framerate ${FR} -f x11grab -s ${RES} -i :${NUMMER}.0+0,0 -vf settb=\(1/30\),setpts=N/TB/30 -r 30 -vcodec libx264 -crf 0 -preset ultrafast -threads 0 ~/wsprfilm/${BAND}.mkv
sleep 60
EOF

chmod 755 /tmp/cmd_${BAND}.sh
xterm -display :${NUMMER} -iconic -e /tmp/cmd_${BAND}.sh




# Extract all images
mkdir -p ~/wsprfilm/${BAND}
cd ~/wsprfilm/${BAND} && rm -rf *
ffmpeg -i ../${BAND}.mkv thumb%08d.jpg -hide_banner
# find a big file for reference
# du -a . | grep jpg | sort -n -r | head -n 10
FILE=$(du -a . | grep jpg | sort -n -r | head -n 100 | tail -n1  | awk '{ print $2}')
SIZE=$(ls -l ${FILE} | awk '{ print $5 }')

# calc 2/3 size of this file 
MIN=$(echo ${SIZE}*0.00075 | bc | cut -f1 -d".")

# delete bad images (small images, e.g. with less details) and create a video
find . -type f -size -${MIN}k -exec rm {} \;
ffmpeg -r 25 -f image2 -s 1920x1080 -pattern_type glob  -i "thumb*.jpg" -vcodec libx264 -crf 25  -pix_fmt yuv420p ${BAND}_flickerfree.mp4

