# WSPR - the film

Create a video of the map on wsprnet.org with simple tools.

First of all, you have to install some applications to your linux computer:

- ffmpeg (a fast video and audio converter)
- kwin (the KDE windows manager. You can use every other windwo manager if you want, but then you have to edit the script)
- xclock
- Xephyr a nested X server 
- bc (the GNU basic calculator)
- xterm (a standard terminal emulator)

All of these application should be avalable in your distribution, they may even be available for solaris.

Next you have to create a new profile for firefox. Even if you want to record several films for several bands: You do not have to create every single profile, you can just clone them.

So, call "firefox -P" to start the profile manager. create a new profile (e.g call it "Profile3"). Then start firefox with this new profile (firefox -P Profile3) and install the "Tab autorefresh plugin" https://addons.mozilla.org/en-US/firefox/addon/tab-auto-refresh. You need this tool to automate the 120s reload of the map. This is because a wspr periode takes 120s. So we expect that we can see something new after 120s.

Then call https://wsprnet.org/drupal/wsprnet/map and log in (you don't have to log in, but the map is wider on the screen if you do). Activate the a 120s reload for the map. Now you can close this firefox instance.

Now it is time to clone some instances. Go to ~/.mozilla/firefox and edit the profile.ini (make a copy for security reasons and don't touch the default profile!). You will see sections like this:

```
[Profile3]
Name=Profile3
IsRelative=1
Path=7vw6elue.Profile3
```

Due the fact that the wspr_film.sh is based on the "[Profile<NUMBER>]" stanza you can modify and clone the other entries, e.g. this way:

```
[Profile3]
Name=80m
IsRelative=1
Path=80m

[Profile4]
Name=40m
IsRelative=1
Path=40m

[Profile5]
Name=20m
IsRelative=1
Path=20m

[Profile6]
Name=10m
IsRelative=1
Path=10m
```

Please avoid Profile numbers less than 3!

After that you have to rename and copy the directory so they fit your values here. For me, I had to execute these commands:

```
renner@solaris:~/.mozilla/firefox$ ls -ltr
drwx------ 11 renner renner 12288 May 28 17:59  7vw6tlue.Profile3
-rw-r--r--  1 renner renner   370 May 28 18:43  profiles.ini
-rw-rw-r--  1 renner renner    55 May 28 18:43  installs.ini
drwx------ 33 renner renner 24576 May 28 18:51  5t7oehqv.default
renner@solaris:~/.mozilla/firefox$ mv 7vw6tlue.Profile3 160m
renner@solaris:~/.mozilla/firefox$ cp -pr 160m 80m
renner@solaris:~/.mozilla/firefox$ cp -pr 160m 40m
renner@solaris:~/.mozilla/firefox$ cp -pr 160m 20m
renner@solaris:~/.mozilla/firefox$ cp -pr 160m 10m
```

Please note, that the tab autoreload plugin and your wsprnet login data are in every profile now, you do not have to login nor install the plugin again.

Now you can start new firefox instances by calling e.g. "firefox -P 80m"

Now have a look to the script to understand what it is doing.

```
BAND=$1

# check if firefox entry exists

ENTRY=$(grep "Name=${BAND}" ~/.mozilla/firefox/profiles.ini)
if [ "${ENTRY}" = "" ] ; then
        echo "no entry ${BAND} found in ~/.mozilla/firefox/profiles.ini"
        echo "please generate one"
        exit 65
else
        echo "entry ${BAND} found in ~/.mozilla/firefox/profiles.ini"
        NUMMER=$(grep -B 1 "Name=${BAND}" ~/.mozilla/firefox/profiles.ini | head -n 1 | sed s/Profile// | sed 's|[^0-9]||g')
        echo "starting screen No ${NUMMER}"
fi
```

wspr_film.sh is started with a parameter, the name of the profile. Like you did before with "80m". The script is checking if there is a coresponding entry in the ~/.mozilla/firefox/profiles.ini and determined the number in the profile name, e.g. 3 from Profile3. If somethin is wrong you will see an error message.

```
RES=2560x1080
FR=0.166666666666666666
Xephyr :${NUMMER} -ac -br -reset -terminate -listen tcp -title "WSPR ${BAND}" -screen ${RES} -reset -terminate &
sleep 5
```

The Resolution for the nested X server is set to 2560x1080. This can be greater or smaller than your "true" screen resolution. The Framerate is set to 0.166666666666666666. This means: in a 120s periode of wspr 20 images are recorded, with other words: every periode is visible for ca. 1s in the resulting video. Then the nested X server is startet as a background job.

Next a temporary script is written to /tmp/

```
cat << EOF > /tmp/cmd_${BAND}.sh

#!/bin/bash
export DISPLAY=:${NUMMER}
kwin &
firefox -P screen0${NUMMER} https://wsprnet.org/drupal/wsprnet/map &
sleep 60
TZ=London xclock -update 60 -render -bg MediumAquamarine -geometry 150x150-90+710 &
rm -f /home/renner/video/${BAND}.mkv
ffmpeg -framerate ${FR} -f x11grab -s ${RES} -i :${NUMMER}.0+0,0 -vf settb=\(1/30\),setpts=N/TB/30 -r 30 -vcodec libx264 -crf 0 -preset ultrafast -threads 0 ~/video/${BAND}.mkv
sleep 60
EOF
```

It set the display number to the value that was found in the profiles.ini. Then the window manager and firefox is started. 60s later a clock is started. It shows UTC time. You need the 60s for the first start. To call wsprnet.org, set the wanted band, zoom factor, coordinates and, if wanted, for activating the Day/Night overlay. If, somehow, the the xclock is not visible you can reduce the window to arrage the clock and miximize the firefox window afterwards. 

Now ffmpeg makes a film with 0.166666666666666666 frames per seconds. This will run forever or till you press "q" in the xterm windows (please do not use CTRL C, because this leads to data loss).

```
# Extract all images
mkdir -p ~/video/${BAND}
cd ~/video/${BAND} && rm -rf *
ffmpeg -i ../${BAND}.mkv thumb%08d.jpg -hide_banner
# find a big file for reference
# du -a . | grep jpg | sort -n -r | head -n 10
FILE=$(du -a . | grep jpg | sort -n -r | head -n 100 | tail -n1  | awk '{ print $2}')
SIZE=$(ls -l ${FILE} | awk '{ print $5 }')

# calc 2/3 size of this file 
MIN=$(echo ${SIZE}*0.00075 | bc | cut -f1 -d".")

# delete bad images (small images, e.g. with less details) and create a video
find . -type f -size -${MIN}k -exec rm {} \;
ffmpeg -r 25 -f image2 -s 1920x1080 -pattern_type glob  -i "thumb*.jpg" -vcodec libx264 -crf 25  -pix_fmt yuv420p ${BAND}_flickerfree.mp4
```

As soon as the last frames are written to the disk all frames are extracted as single images. This will take a while. But we have to remove bad images. Frames that where taken while the page was reloading. This can be seen quite well from the file size. So if a file is less than 75% of a average file size it is removed to avoid flickering in the film.
The remaining images are converted to a video again, in case of the 80m profile in ~/video/40m_flickerfree.mp4

Now start recording two videos, e.g. one for 40m and one for 80m. After 24h type q into the terminal window and wait till your PC cool down :-)

Enjoy!









